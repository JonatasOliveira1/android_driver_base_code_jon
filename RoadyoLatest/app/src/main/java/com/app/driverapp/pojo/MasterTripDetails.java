package com.app.driverapp.pojo;

import java.io.Serializable;

/**
 * Created by embed on 7/4/16.
 */
public class MasterTripDetails implements Serializable {

    private String errNum;

    private PaymentLoc paymentLoc;

    private Trips Trips;

    private String errMsg;

    private String errFlag;

    public String getErrNum ()
    {
        return errNum;
    }

    public void setErrNum (String errNum)
    {
        this.errNum = errNum;
    }

    public PaymentLoc getPaymentLoc ()
    {
        return paymentLoc;
    }

    public void setPaymentLoc (PaymentLoc paymentLoc)
    {
        this.paymentLoc = paymentLoc;
    }

    public Trips getTrips ()
    {
        return Trips;
    }

    public void setTrips (Trips Trips)
    {
        this.Trips = Trips;
    }

    public String getErrMsg ()
    {
        return errMsg;
    }

    public void setErrMsg (String errMsg)
    {
        this.errMsg = errMsg;
    }

    public String getErrFlag ()
    {
        return errFlag;
    }

    public void setErrFlag (String errFlag)
    {
        this.errFlag = errFlag;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [errNum = "+errNum+", paymentLoc = "+paymentLoc+", Trips = "+Trips+", errMsg = "+errMsg+", errFlag = "+errFlag+"]";
    }
}
