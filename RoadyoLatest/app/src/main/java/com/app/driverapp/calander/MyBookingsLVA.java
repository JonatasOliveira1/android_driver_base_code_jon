package com.app.driverapp.calander;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.app.driverapp.MainActivity;
import com.app.driverapp.R;
import com.app.driverapp.response.UpdateAppointmentDetail;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 * Created by PrashantSingh on 2/11/16.
 */
public class MyBookingsLVA extends ArrayAdapter<AppointmentDtlsData>
{
    private final Typeface font,fontBold;
    private Context mContext;
    private ArrayList<AppointmentDtlsData> aptDtlsData;
    private Typeface openSansRegular;

    /*******************************************************/

    public MyBookingsLVA(Context context, int resource, ArrayList<AppointmentDtlsData> aptDtlsData)
    {
        super(context, R.layout.appointmentlistviewitem, aptDtlsData);
        this.mContext = context;
        this.aptDtlsData = aptDtlsData;

        font = Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Regular.ttf");
        fontBold = Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf");
       // openSansRegular = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
    }
    /*******************************************************/

    @Override
    public int getCount()
    {
        return aptDtlsData.size();
    }
    /*******************************************************/

    @Override
    public AppointmentDtlsData getItem(int position)
    {
        return aptDtlsData.get(position);
    }
    /*******************************************************/

    private class ViewHolder
    {
        TextView  passenger_name,BookingId;
        TextView pick_location;
        TextView status;
        TextView drop_location;
        TextView amount,amount_text;
        TextView time_text;
        TextView pick_loc_text;
        TextView drop_loc_text;
    }
    /*******************************************************/

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        AppointmentDtlsData aptDtlsDataItem = aptDtlsData.get(position);

        ViewHolder holder;
        if(convertView==null||convertView.getTag()==null)
        {
            holder = new ViewHolder();
            // inflate the layout
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.single_row_appointment_list, parent, false);

            holder.passenger_name=(TextView)convertView.findViewById(R.id.passenger_name);
            holder.amount = (TextView)convertView.findViewById(R.id.amount);
            holder.amount_text = (TextView)convertView.findViewById(R.id.amount_text);
            holder.pick_location=(TextView)convertView.findViewById(R.id.pick_loc);
            holder.drop_location=(TextView)convertView.findViewById(R.id.drop_loc);
            holder.time_text=(TextView)convertView.findViewById(R.id.time_text);
            holder.status = (TextView)convertView.findViewById(R.id.status);
            holder.BookingId = (TextView)convertView.findViewById(R.id.home_booking_id_text);
            holder.pick_loc_text = (TextView)convertView.findViewById(R.id.pick_loc_text);
            holder.drop_loc_text = (TextView)convertView.findViewById(R.id.drop_loc_text);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }


        String amountstr = VariableConstants.CURRENCY_SYMBOL+aptDtlsDataItem.getAmount();


        holder.passenger_name.setId(position);
        //holder.imageview.setId(position);
        holder.pick_location.setId(position);
        holder.drop_location.setId(position);
        //holder.petientnameandtimelayout.setId(position);
        //holder.appointmentitemmainlayout.setId(position);
        holder.time_text.setId(position);
        holder.amount.setId(position);
        holder.amount_text.setId(position);
        holder.status.setId(position);
        holder.BookingId.setId(position);
        holder.pick_loc_text.setId(position);
        holder.drop_loc_text.setId(position);

        holder.passenger_name.setTypeface(fontBold);
        holder.pick_location.setTypeface(font);
        holder.drop_location.setTypeface(font);
        holder.time_text.setTypeface(font);
        holder.amount.setTypeface(fontBold);
        holder.amount_text.setTypeface(fontBold);
        holder.status.setTypeface(fontBold);
        holder.BookingId.setTypeface(font);
        holder.pick_loc_text.setTypeface(font);
        holder.drop_loc_text.setTypeface(font);

        holder.passenger_name.setText(aptDtlsDataItem.getFname());
        holder.pick_location.setText(" "+aptDtlsDataItem.getAddrLine1());
        //holder.destanceunittextview.setText(" "+aptDtlsDataItem.getDistance()+" "+mContext.getResources().getString(R.string.kmh));
        holder.time_text.setText(mContext.getResources().getString(R.string.time_text)+" "+aptDtlsDataItem.getApntTime());
        holder.drop_location.setText(" "+aptDtlsDataItem.getDropLine1());
        holder.amount.setText(amountstr);
        holder.BookingId.setText(mContext.getResources().getString(R.string.bookingidtext)+" "+aptDtlsDataItem.getBid());
        if ("0".equals(aptDtlsDataItem.getPayStatus()))
        {
            holder.amount_text.setText("" + mContext.getResources().getString(R.string.paymentnot));
        }
        else if ("1".equals(aptDtlsDataItem.getPayStatus()))
        {
            holder.amount_text.setText("" + mContext.getResources().getString(R.string.paydone));
        }
        else if ("2".equals(aptDtlsDataItem.getPayStatus()))
        {
            holder.amount_text.setText(""+mContext.getResources().getString(R.string.dispute));
        }
        else
        {
            holder.amount_text.setText(""+mContext.getResources().getString(R.string.closed));
        }

        holder.status.setText(" "+aptDtlsDataItem.getStatus().toUpperCase());
        return convertView;
    }
    /*******************************************************/


}
