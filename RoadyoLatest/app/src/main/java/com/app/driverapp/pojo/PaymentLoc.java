package com.app.driverapp.pojo;

import java.io.Serializable;

/**
 * Created by embed on 7/4/16.
 */
public class PaymentLoc implements Serializable{

    private String LastPayAmount;

    private String DriverTotalEarning;

    private String DriverCardEarning;

    private String DriverToPay;

    private String pending;

    private String LastPayDate;

    private String DriverCashEarning;

    private String DriverCashColleted;

    private String TotalDue;

    private String TotalReceived;

    public String getLastPayAmount ()
    {
        return LastPayAmount;
    }

    public void setLastPayAmount (String LastPayAmount)
    {
        this.LastPayAmount = LastPayAmount;
    }

    public String getDriverTotalEarning ()
    {
        return DriverTotalEarning;
    }

    public void setDriverTotalEarning (String DriverTotalEarning)
    {
        this.DriverTotalEarning = DriverTotalEarning;
    }

    public String getDriverCardEarning ()
    {
        return DriverCardEarning;
    }

    public void setDriverCardEarning (String DriverCardEarning)
    {
        this.DriverCardEarning = DriverCardEarning;
    }

    public String getDriverToPay ()
    {
        return DriverToPay;
    }

    public void setDriverToPay (String DriverToPay)
    {
        this.DriverToPay = DriverToPay;
    }

    public String getPending ()
    {
        return pending;
    }

    public void setPending (String pending)
    {
        this.pending = pending;
    }

    public String getLastPayDate ()
    {
        return LastPayDate;
    }

    public void setLastPayDate (String LastPayDate)
    {
        this.LastPayDate = LastPayDate;
    }

    public String getDriverCashEarning ()
    {
        return DriverCashEarning;
    }

    public void setDriverCashEarning (String DriverCashEarning)
    {
        this.DriverCashEarning = DriverCashEarning;
    }

    public String getDriverCashColleted ()
    {
        return DriverCashColleted;
    }

    public void setDriverCashColleted (String DriverCashColleted)
    {
        this.DriverCashColleted = DriverCashColleted;
    }

    public String getTotalDue() {
        return TotalDue;
    }

    public void setTotalDue(String totalDue) {
        TotalDue = totalDue;
    }

    public String getTotalReceived()
    {
        return TotalReceived;
    }

    public void setTotalReceived(String totalReceived)
    {
        TotalReceived = totalReceived;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LastPayAmount = "+LastPayAmount+", DriverTotalEarning = "+DriverTotalEarning+", DriverCardEarning = "+DriverCardEarning+", DriverToPay = "+DriverToPay+", pending = "+pending+", LastPayDate = "+LastPayDate+", DriverCashEarning = "+DriverCashEarning+", DriverCashColleted = "+DriverCashColleted+"]";
    }
}
