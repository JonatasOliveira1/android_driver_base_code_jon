package com.app.driverapp.utility;

public class VariableConstants 
{

	
	public static final String PUB_KEY = "";
	public static final String SUB_KEY = "";


	
	
	public static final String hostUrl="";
	public static final String ImageUrl="";
	/**
	 * Needs to change file name
	 */
	//File name for profile picture during Sign-up
	public static final String TEMP_PHOTO_FILE_NAME = "temp_pic.jpg";
	//public static final String PROJECT_ID="150824869972";
	public static final String PROJECT_ID="196780370804";

	public static final String TERMSLINK="";
	public static final String PRIVACYLINK="";

	public static final String FACEBOOKLINK="https://www.facebook.com/";
	public static final String WEBSITE="roadyo.in";

	public static final String GET_COMPANY_TYPE=hostUrl+"getTypes";
	public static final String MASTER_SIGNUP_STEP_1 =hostUrl+"masterSignup1";
	public static final String getAppointmentDetail_url=hostUrl+"getMasterAppointments";
    public static final String uploadImage_url=hostUrl+"uploadImage";
    public static final String getRejectstatusUpdate_url=hostUrl+"respondToAppointment";
    public static final String getAppointmenttatus_url=hostUrl+"getApptStatus";
	public static final String getAppointmentstatusUpdate_url=hostUrl+"updateApptStatus";
	public static final String getUpdateAppointmentDetail_url=hostUrl+"updateApptDetails";
	public static final String getAppointmentDetailhistory_url=hostUrl+"getHistoryWith";
	public static final String getAbortJourney_url=hostUrl+"abortJourney";
	public static final String getProfileinfo_url=hostUrl+"getMasterProfile";
	public static final String logOut_url=hostUrl+"logout";
	public static final String Resetpassword_url=hostUrl+"resetPassword";
	public static final String Forgotpassword=hostUrl+"forgotPassword";
	public static final String getAppointmentDetails_url=hostUrl+"getAppointmentDetails";
	public static final String getMasterStatus_url=hostUrl+"updateMasterStatus";
	public static final String getMasterLocation_url=hostUrl+"updateMasterLocation";
	public static final String getPandingBooking_url=hostUrl+"getPendingRequests";
	
	public static final String methodeName="POST";
	public static final String PREF_NAME = "DriverApp";
	public static final String PASSENGERAPPOINMENTDATEANDTIME="passengerappointmentdateandtime"; 
	public static final String APPOINTMENT="appointment"; 
	public static final String LOGINERRORFLAG="loginerrorflag";
	public static final String LOGINERRORNUM="loginerrornum";
	public static final String LOGINERRORMESSAGE="loginerrormessage";
	//public static final int REQUEST_FOR_GETLOCATIONFROMSEARCH=200;
	public static final String FLLURY_ANALYTICS_ID="T9DR5YFK3P3W8YXJHGBJ";
	public static final String USERTYPE="1";
	public static final String APT_DTLS_INTENT = "appointmentDetails";
	public static int updatelocationUpdateTimerTiem = 2000;// 2 second

	public static final String CURRENCY_SYMBOL = "$";


	public static String GOOGLE_SERVER_API_KEY="AIzaSyBmudNaGSa4bcR-PeEqy52Z_SuB4AGJzqA";
	//public static String GOOGLE_SERVER_API_KEY="AIzaSyASnskkj3Jw1L9lxtLyTH-HMdALNoFG3x8";
}
